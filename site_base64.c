#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>

// gcc site_base64.c -o site_base64

void encode_base64(const char *input, char *output) {
    static const char encoding_table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    int input_length = strlen(input);
    int output_length = 4 * ((input_length + 2) / 3);
    
    for (int i = 0, j = 0; i < input_length;) {
        uint32_t octet_a = i < input_length ? (unsigned char)input[i++] : 0;
        uint32_t octet_b = i < input_length ? (unsigned char)input[i++] : 0;
        uint32_t octet_c = i < input_length ? (unsigned char)input[i++] : 0;
        
        uint32_t triple = (octet_a << 0x10) + (octet_b << 0x08) + octet_c;
        
        output[j++] = encoding_table[(triple >> 3 * 6) & 0x3F];
        output[j++] = encoding_table[(triple >> 2 * 6) & 0x3F];
        output[j++] = encoding_table[(triple >> 1 * 6) & 0x3F];
        output[j++] = encoding_table[(triple >> 0 * 6) & 0x3F];
    }
    
    for (int i = 0; i < 3 - (input_length % 3); i++) {
        output[output_length - 1 - i] = '=';
    }
}

int main(int argc, char *argv[]) {
    if (argc != 3) {
        printf("Usage: %s input_file output_file\n", argv[0]);
        return 1;
    }
    
    const char *input_file = argv[1];
    const char *output_file = argv[2];
    
    FILE *input = fopen(input_file, "r");
    if (input == NULL) {
        fprintf(stderr, "Error opening input file: %s\n", input_file);
        return 1;
    }
    
    fseek(input, 0, SEEK_END);
    long input_size = ftell(input);
    fseek(input, 0, SEEK_SET);
    
    char *input_buffer = (char *)malloc(input_size);
    if (input_buffer == NULL) {
        fclose(input);
        fprintf(stderr, "Error allocating memory for input file\n");
        return 1;
    }
    
    fread(input_buffer, 1, input_size, input);
    fclose(input);
    
    char *base64_buffer = (char *)malloc(4 * ((input_size + 2) / 3) + 1);
    if (base64_buffer == NULL) {
        free(input_buffer);
        fprintf(stderr, "Error allocating memory for base64 encoding\n");
        return 1;
    }
    
    encode_base64(input_buffer, base64_buffer);
    
    FILE *output = fopen(output_file, "w");
    if (output == NULL) {
        free(input_buffer);
        free(base64_buffer);
        fprintf(stderr, "Error opening output file: %s\n", output_file);
        return 1;
    }
    
    fprintf(output, "<script>\n");
    fprintf(output, "  document.write(atob('");
    fprintf(output, "%s", base64_buffer);
    fprintf(output, "'));\n");
    fprintf(output, "</script>\n");
    
    fclose(output);
    free(input_buffer);
    free(base64_buffer);
    
    printf("HTML obfuscated and saved to %s\n", output_file);
    
    return 0;
}
